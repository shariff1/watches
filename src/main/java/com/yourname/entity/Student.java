package com.yourname.entity;

public class Student {
    private Integer id;
    private String brand;
    private String model;
    private String caseSize;
    private String caseMaterial;
    private String braclet;
    private String price;
    private String sku;
    private String condition;
    private String images;


    public Student() {
    }

    public Student( Integer id, String brand, String model, String caseSize,
                   String caseMaterial, String braclet, String price,
                   String sku, String condition, String images) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.caseSize = caseSize;
        this.caseMaterial = caseMaterial;
        this.braclet = braclet;
        this.price = price;
        this.sku = sku;
        this.condition = condition;
        this.images = images;

    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getCaseSize() {
        return caseSize;
    }

    public void setCaseSize(String caseSize) {
        this.caseSize = caseSize;
    }

    public String getCaseMaterial() {
        return caseMaterial;
    }

    public void setCaseMaterial(String caseMaterial) {
        this.caseMaterial = caseMaterial;
    }

    public String getBraclet() {
        return braclet;
    }

    public void setBraclet(String braclet) {
        this.braclet = braclet;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
